import numpy as np
import cv2
import pandas as pd

def calcCircleLevel(img_m):
    gray =  cv2.cvtColor(img_m, cv2.COLOR_BGR2GRAY)
    images, contours, hierarchy = cv2.findContours(gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    cnt = contours[0]
    area = cv2.contourArea(cnt)
    perimeter = cv2.arcLength(cnt, True)
    if(perimeter==0):
        return 0
    circle_level = 4.0 * np.pi * area / (perimeter * perimeter)
    return circle_level


#各Imageについて

# ImageIDファイルを読み込む
imageIDs = pd.read_csv("./workplace/ImageID_car_wheel.txt",header=None) # 車とタイヤの写った画像のImageIDが並んだファイル
annotation_b = pd.read_csv("./annotation/train-annotations-bbox.csv") # 全てのannotaion
annotation_cw = annotation_b[annotation_b["ImageID"].isin(imageIDs[0])]# imageIDがimgeIDsに含まれるものだけ
wheel_id = "/m/083wq"
annotation_w = annotation_cw[annotation_cw["LabelName"] == wheel_id]# ラベルがタイヤのものだけ
annotation_w = annotation_w.reset_index(drop=True)
annotation_s = pd.read_csv("./annotation/train-annotations-object-segmentation.csv") # 全てのannotaion

target = pd.DataFrame(annotation_w["ImageID"])
target.rename(columns={0:"ImageID"})
target["BoxXSize"] = None
target["PicXSize"] = None
target["BoxYSize"] = None
target["PicYSize"] = None
target["SegFlag"] = False
target["CircleLevel"] = None


count = 0
l = len(annotation_w)
for index, row in annotation_w.iterrows():
    count+=1
    # 元の画像は"./Images/{ImageID}.jpg"
    img = cv2.imread("./Car_Wheel_Images/{0}.jpg".format(row["ImageID"]))
    y,x = img.shape[:2]
    sizes = np.array([row["XMin"],row["XMax"],row["YMin"],row["YMax"]])*np.array([x,x,y,y])
    sizes = sizes.astype("int64")
    x_min,x_max,y_min,y_max = sizes # Boxの位置(pixel)

    target.loc[index,"PicXSize"] = x
    target.loc[index,"PicYSize"] = y
    target.loc[index,"BoxXSize"] = x_max-x_min
    target.loc[index,"BoxYSize"] = y_max-y_min


    row_s = annotation_s[(annotation_s["BoxXMin"]==row["XMin"]) & (annotation_s["BoxYMin"]==row["YMin"]) & (annotation_s["BoxXMax"]==row["XMax"]) & (annotation_s["BoxYMax"]==row["YMax"]) ]
    if(len(row_s) != 0):
        try:
            target.loc[index,"SegFlag"] = True
            mpath = row_s["MaskPath"]
            img_m = cv2.imread("./mask_Wheel_Images/{0}".format(mpath.values[0]))
            target.loc[index,"CircleLevel"] = calcCircleLevel(img_m)
        except:
            print("Error happened, but for now ignore it")
            pass
    print("{0}".format(count/l))
target.to_csv("annotation-Car-Wheel.csv",index=False)
