# toyota-gan-openimagesV5

Open Images V5をダウンロードする(from Google)。


## Open Images Dataset
***
### 概要：
google発の画像データセット(2019/05/)。600クラス、172万画像。
識別されたオブジェクト(の多く)にはセグメンテーションのデータ、relational data(\[誰が、どうした、何を\]、今回使わない)も用意されている
### 利用方法：


## ダウンロードの流れ
***
全てダウンロードしてしまうと600GBくらい食ってしまうので、今回は既にラベルづけられているクラスごとにダウンロードすることを考えます。

アノテーション関係のデータをまず[ここのリンク先]からとってくる。
Imagesは重いデータなのでそれ以外をwgetでダウンロード。以下ではtrainデータのみ回収する。(全部は使わない)

```sh
# ボックス：　検知したオブジェクトを囲む四角の位置情報
wget https://storage.googleapis.com/openimages/2018_04/train/train-annotations-bbox.csv
# セグメンテーション：　検知したオブジェクトだけを囲む領域を表す情報
wget https://storage.googleapis.com/openimages/v5/train-annotations-object-segmentation.csv
# Image labels：　オブジェクトのラベル
wget https://storage.googleapis.com/openimages/v5/train-annotations-human-imagelabels-boxable.csv
# Image ID：　各写真の管理番号、今回はまだ使ってない
wget https://storage.googleapis.com/openimages/2018_04/train/train-images-boxable-with-rotation.csv

# metadata：　クラスの種類、各種フラグ等、challengeと書いてあるのは今回使わないかも
mkdir metadata
cd metadata
wget https://storage.googleapis.com/openimages/v5/class-descriptions-boxable.csv
wget https://storage.googleapis.com/openimages/2019_01/challenge-2018-relationships-description.csv
wget https://storage.googleapis.com/openimages/2019_01/challenge-2018-attributes-description.csv
wget https://storage.googleapis.com/openimages/2019_01/challenge-2018-relationship-triplets.csv
wget https://storage.googleapis.com/openimages/v5/classes-segmentation.txt
```

Imageデータは画像1枚ごとダウンロードできるので、容量の関係から画像が欲しい時は先にダウンロードurlファイルを作成し、wget -i ファイルでダウンロードが良いと思われる
```sh
wget -i url_list.txt
```

# 作業目的
車のタイヤが写った画像を抽出し、

## ディレクトリ構造(マスターブランチ、2019/10/14現在)
***
### openimagev5/
*   annotation/
： 主にcsv形式で、ImageID、BoxSize、ラベル等の結果をまとめたデータ置き場
*   Car_Wheel_Images/
： CarラベルとWheelラベルの二つが入った画像ファイル群(__大量、開くの注意__)
*   mask_Wheel_Images/
： タイヤのセグメンテーション(輪郭に沿ってくり抜いてあるやつ)画像ファイル群(__大量、開くの注意__)。該当部分だけが白、それ以外が黒で塗りつぶされてる。__元の画像とサイズが違うので注意(セグメンテーションで切り抜いた画像を利用する時は等倍して使うこと)__
*   workplace/
： ここも作業場です。藤本がデータまとめるのに使ったpythonファイル、車とタイヤの写った画像のurlリストとか雑多に入ってます。
### sandbox/
* 作業を試したりする。好きに使ってます。消した方がいいかも...

## やったこと
## 〜tips〜
+ 前の標準出力を次の{}の位置に代入して次コマンド実行
例）xargs -I{} grep {} train-annotations-bbox.csv 
+ ラベル名からラベルのIDを獲得
例）grep -w 'Wheel' class-descriptions-boxable.csv | cut -d , -f1
+ ラベルIDからImageID
例）xargs -I{} grep {} train-annotations-bbox.csv | cut -d , -f1
+ ImageIDから写真のurl
例）xargs -I{} grep {} train-images-boxable.csv | cut -d , -f2